let collection = [];

// Write the queue functions below.


function print() {
	return collection;
}

function enqueue(element) {
	collection[collection.length] = element;
	return collection
}

function dequeue() {
	let newCollection = [];

	for (x = 0; x < collection.length -1; x++) {
		newCollection[x] = collection[x+1];
	}
	return collection = newCollection
}

function front() {
	return collection[0];
}

function size() {
	return collection.length
}

function isEmpty() {
	return (collection.length !== 0)
	?
		false
	:
		true;
}


module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};